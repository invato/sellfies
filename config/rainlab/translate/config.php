<?php

return [
  'prefixDefaultLocale' => false,
  'cacheTimeout' => 1,
  'disableLocalePrefixRoutes' => true,
];