const colors = require('tailwindcss/colors')

module.exports = {
    content: [
        './**/*.htm',
        '../../plugins/**/*.htm',
    ],
    safelist: [
        'bg-white',
        'bg-snow',
        'bg-primary-100',
        'bg-secondary-100',
        'max-w-3xl',
        'max-w-4xl',
        'max-w-5xl',
        'max-w-6xl',
        'max-w-7xl',
    ],
    theme: {
        screens: {
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1280px',
        },
        container: {
            center: true,
            padding: '1rem',
            screens: {
                'sm': '640px',
                'md': '768px',
                'lg': '1024px',
                'xl': '1280px',
            }
        },
        extend: {
            fontFamily: {
                'inter': ['Inter', 'sans-serif'],
                'raleway': ['Raleway', 'sans-serif'],
                'kurye': ['Kurye', 'Raleway', 'sans-serif'],
                'bench': ['BenchNine', 'Kurye', 'sans-serif'],
            },
            fontSize: {
                title: ['2.5rem', '1'],
                display: ['5rem', '1'],
                display_xl: ['6rem', '1'],
            },
            colors: {
                primary: {
                    DEFAULT: '#850199',
                    '100': '#eadfff',
                    '200': '#7c459a',
                    '300': '#850199',
                    '400': '#461c4c',
                },
                secondary: {
                    DEFAULT: '#f49ebc',
                    '100': '#f8e8ee',
                    '200': '#facddf',
                    '300': '#f49ebc',
                    '400': '#f58cb0',
                    '500': '#ec7fa4',
                },
                text: '#252525',
                lead: '#2f2f2f',
                ash: '#979797',
                antracite: '#606060',
                silver: {
                    DEFAULT: '#f0f0f0',
                    '100': '#f6f6f6',
                    '200': '#ebebeb',
                },
                snow: '#f8f8f8',
            },
            boxShadow: {
                'float': '0 1px 19px 0 rgba(244, 129, 255, 0.28)',
                'float-hover': '0 1px 19px 0 rgba(244, 129, 255, 0.58)',
                'float-black': '0 2px 9px 0 rgba(0,0,0,.44)',
                'purple': '1px 13px 17px 0 rgba(86, 58, 144, 0.27)',
            }
        },
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/forms'),
    ],
}
