<?php namespace Invato\Sellfies\Components;

use Invato\Sellfies\Models\Testimonial;
use Cms\Classes\ComponentBase;
use Db;

class Slider extends ComponentBase
{
    /**
     * A collection of records to display
     * @var \October\Rain\Database\Collection
     */
    public $testimonials;


    public function componentDetails()
    {
        return [
            'name'        => 'Testimonial slider',
            'description' => 'Shows last 4 testimonials in a carousel slider'
        ];
    }

    //
    // Properties
    //

    public function defineProperties()
    {
        return [];
    }
    //
    // Rendering and processing
    //

    public function onRun()
    {
        $this->testimonials = Testimonial::orderBy('sort_order')->get();
    }
}
