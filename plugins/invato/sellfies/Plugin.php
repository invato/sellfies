<?php namespace Invato\Sellfies;
use Event;
use System\Classes\PluginBase;
use System\Classes\PluginManager;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Invato\Sellfies\Components\Slider' => 'slider',
        ];
    }

    public function registerSettings()
    {
    }

    private function getIndexOptions()
    {
        return ["index"=>"index","noindex"=>"noindex"];
    }

    private function getFollowOptions()
    {
        return ["follow"=>"follow","nofollow"=>"nofollow"];
    }

    public function boot() {
        \Cms\Models\ThemeData::extend(function ($model) {
            $model->addJsonable('company');
            $model->addJsonable('social');
        });

        \RainLab\Blog\Models\Post::extend(function ($model){
            $model->addJsonable('viewBag');
        });

        Event::listen('backend.form.extendFields', function($widget) {

            if (
                !$widget->getController() instanceof \RainLab\Pages\Controllers\Index ||
                !$widget->model instanceof \RainLab\Pages\Classes\Page
            ) {
                return;
            }
            if (!$widget->isNested) {
                $widget->addTabFields([
                    'viewBag[seo_title]' => [
                        'label'   => 'Meta Title',
                        'type'    => 'text',
                        'tab'     => 'SEO'
                    ],
                    'viewBag[seo_description]' => [
                        'label'   => 'Meta Description',
                        'type'    => 'textarea',
                        'size'    => 'tiny',
                        'tab'     => 'SEO'
                    ],
                    'viewBag[seo_keywords]' => [
                        'label'   => 'Meta Keywords',
                        'type'    => 'textarea',
                        'size'    => 'tiny',
                        'tab'     => 'SEO'
                    ],
                    'viewBag[canonical_url]' => [
                        'label'   => 'Canonical URL',
                        'type'    => 'text',
                        'tab'     => 'SEO',
                        'span'    => 'left'
                    ],
                    'viewBag[robot_index]' => [
                        'label'   => 'Robot Index',
                        'type'    => 'dropdown',
                        'tab'     => 'SEO',
                        'options' => $this->getIndexOptions(),
                        'default' => 'index',
                        'span'    => 'left'
                    ],
                    'viewBag[robot_follow]' => [
                        'label'   => 'Robot Follow',
                        'type'    => 'dropdown',
                        'tab'     => 'SEO',
                        'options' => $this->getFollowOptions(),
                        'default' => 'follow',
                        'span'    => 'right'
                    ]
                ]);

                $widget->removeField('viewBag[meta_title]');
                $widget->removeField('viewBag[meta_description]');
            }
        });

        Event::listen('backend.form.extendFields', function ($widget) {

            if (
                !$widget->getController() instanceof \RainLab\Blog\Controllers\Posts ||
                !$widget->model instanceof \RainLab\Blog\Models\Post
            ) {
                return;
            }

            if ($widget->isNested) {
                return;
            }

            $widget->addSecondaryTabFields([
                'viewBag[seo_title]' => [
                    'label'   => 'Meta Title',
                    'type'    => 'text',
                    'tab'     => 'SEO'
                ],
                'viewBag[seo_description]' => [
                    'label'   => 'Meta Description',
                    'type'    => 'textarea',
                    'size'    => 'tiny',
                    'tab'     => 'SEO'
                ],
                'viewBag[seo_keywords]' => [
                    'label'   => 'Meta Keywords',
                    'type'    => 'textarea',
                    'size'    => 'tiny',
                    'tab'     => 'SEO'
                ],
                'viewBag[canonical_url]' => [
                    'label'   => 'Canonical URL',
                    'type'    => 'text',
                    'tab'     => 'SEO',
                    'span'    => 'left'
                ],
                'viewBag[redirect_url]' => [
                    'label'   => 'Redirect URL',
                    'type'    => 'text',
                    'tab'     => 'SEO',
                    'span'    => 'right'

                ],
                'viewBag[robot_index]' => [
                    'label'   => 'Robot Index',
                    'type'    => 'dropdown',
                    'tab'     => 'SEO',
                    'options' => $this->getIndexOptions(),
                    'default' => 'index',
                    'span'    => 'left'
                ],
                'viewBag[robot_follow]' => [
                    'label'   => 'Robot Follow',
                    'type'    => 'dropdown',
                    'tab'     => 'SEO',
                    'options' => $this->getFollowOptions(),
                    'default' => 'follow',
                    'span'    => 'right'
                ],
            ]);
        });
    }
}
