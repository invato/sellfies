<?php namespace Invato\Sellfies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateInvatoSellfiesTestimonials2 extends Migration
{
    public function up()
    {
        Schema::table('invato_sellfies_testimonials', function($table)
        {
            $table->string('avatar');
        });
    }
    
    public function down()
    {
        Schema::table('invato_sellfies_testimonials', function($table)
        {
            $table->dropColumn('avatar');
        });
    }
}
