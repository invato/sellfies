<?php namespace Invato\Sellfies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateInvatoSellfiesTestimonials extends Migration
{
    public function up()
    {
        Schema::create('invato_sellfies_testimonials', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('name');
            $table->string('country');
            $table->text('testimonial');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('sort_order')->default(0);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('invato_sellfies_testimonials');
    }
}
