<?php namespace Sellfies\Settings\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class createBlogViewbagField extends Migration
{
    public function up()
    {
        Schema::table('rainlab_blog_posts', function($table)
        {
            $table->text('viewBag')->nullable();
        });
    }

    public function down()
    {
        Schema::table('rainlab_blog_posts', function($table)
        {
            $table->dropColumn('viewBag');
        });
    }
}
