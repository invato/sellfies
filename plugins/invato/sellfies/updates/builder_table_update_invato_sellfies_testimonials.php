<?php namespace Invato\Sellfies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateInvatoSellfiesTestimonials extends Migration
{
    public function up()
    {
        Schema::table('invato_sellfies_testimonials', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('invato_sellfies_testimonials', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}
