<?php

//use \Esbrin\Webshops\Models\Shops;
//
//Route::get('get_webshops', function() {
//    $shops = Shops::all();
//    return $shops;
//});

if(class_exists('\\Rainlab\\Pages\\Classes\\Menu')) {
    Route::get('api/get-menus', function() {
        $theme = \Cms\Classes\Theme::getActiveTheme();
        $menus = \RainLab\Pages\Classes\Menu::listInTheme($theme, 'Mainmenu');
        $menu_full = json_decode($menus);
        return getRecursiveMenu($menus);
    });

    Route::get('api/{language}/get-menus', function($language) {
        $theme = \Cms\Classes\Theme::getActiveTheme();
        $menus = \RainLab\Pages\Classes\Menu::listInTheme($theme, 'Mainmenu');
        $menu_full = json_decode($menus);
        return getRecursiveMenuByLanguage($language, $menus);
    });
}

function getRecursiveMenu($menus) {
    static $level = 0;
    static $next_level = 0;
    $menuDetails = [];
    foreach($menus as $iMenu) {
        $detail = [];
        if ($level == $next_level) {
            $detail['menu_name'] = ['name' => $iMenu->name];
        } else
            $detail['menu_name'] = ['title' => $iMenu->title, 'url' => $iMenu->url ? $iMenu->url : "/" . $iMenu->reference];

        $level++;
        $items = getRecursiveMenu($iMenu->items);

        if(count($items) > 0 ) {
            $detail['menu_items'] = $items;
        }
        $menuDetails[] = $detail;
        $next_level++;
    }
    return $menuDetails;
}
function getRecursiveMenuByLanguage($language, $menus) {
    static $level = 0;
    static $next_level = 0;
    $menuDetails = [];
    foreach($menus as $iMenu) {
        $detail = [];
        if ($level == $next_level) {
            $detail['menu_name'] = ['name' => $iMenu->name];
        } else
            if (array_key_exists('locale', $iMenu->viewBag) && isset($iMenu->viewBag['locale'][$language])) {
                $detail['menu_name'] = ['title' => $iMenu->viewBag['locale'][$language]['title'], 'url' => $iMenu->viewBag['locale'][$language]['url']];
            } else
                $detail['menu_name'] = ['title' => $iMenu->title, 'url' => $iMenu->url ? $iMenu->url : "/" . $iMenu->url];

        $level++;
        $items = getRecursiveMenuByLanguage($language, $iMenu->items);

        if(count($items) > 0 ) {
            $detail['menu_items'] = $items;
        }
        $menuDetails[] = $detail;
        $next_level++;
    }
    return $menuDetails;
}
