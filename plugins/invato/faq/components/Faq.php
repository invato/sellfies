<?php namespace Invato\Faq\Components;

use Invato\Faq\Models\Category;
use Invato\Faq\Models\Question;
use Cms\Classes\ComponentBase;
use Db;

class Faq extends ComponentBase
{
    /**
     * A collection of records to display
     * @var \October\Rain\Database\Collection
     */
    public $categories;
    public $isLoggedIn;


    public function componentDetails()
    {
        return [
            'name'        => 'Frequently asked questions',
            'description' => 'Shows the faq component'
        ];
    }

    //
    // Properties
    //

    public function defineProperties()
    {
        return [];
    }
    //
    // Rendering and processing
    //

    public function onRun()
    {
        $this->isLoggedIn = false;
        if ( isset($_COOKIE['IS_LOGGED_IN']) ) {
            $this->isLoggedIn = true;
        }

        $this->categories = Category::orderBy('sort_order')->get();
        // $this->categories = Category::orderBy('sort_order')->get();
    }
}
