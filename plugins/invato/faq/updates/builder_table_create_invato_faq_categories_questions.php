<?php namespace Invato\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateInvatoFaqCategoriesQuestions extends Migration
{
    public function up()
    {
        Schema::create('invato_faq_categories_questions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('categories_id')->unsigned();
            $table->integer('questions_id')->unsigned();
            $table->primary(['categories_id', 'questions_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('invato_faq_categories_questions');
    }
}
