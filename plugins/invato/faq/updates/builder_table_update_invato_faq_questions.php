<?php namespace Invato\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateInvatoFaqQuestions extends Migration
{
    public function up()
    {
        Schema::table('invato_faq_questions', function($table)
        {
            $table->integer('category_id');
        });
    }
    
    public function down()
    {
        Schema::table('invato_faq_questions', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
}
