<?php namespace Invato\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateInvatoFaqCategories extends Migration
{
    public function up()
    {
        Schema::create('invato_faq_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->integer('sort_order')->nullable()->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('questions_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('invato_faq_categories');
    }
}
