<?php namespace Invato\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateInvatoFaqQuestions2 extends Migration
{
    public function up()
    {
        Schema::table('invato_faq_questions', function($table)
        {
            $table->integer('categories_id')->default(0);
            $table->dropColumn('category_id');
        });
    }
    
    public function down()
    {
        Schema::table('invato_faq_questions', function($table)
        {
            $table->dropColumn('categories_id');
            $table->integer('category_id');
        });
    }
}
