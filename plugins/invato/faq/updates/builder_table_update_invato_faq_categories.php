<?php namespace Invato\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateInvatoFaqCategories extends Migration
{
    public function up()
    {
        Schema::table('invato_faq_categories', function($table)
        {
            $table->dropColumn('questions_id');
        });
    }
    
    public function down()
    {
        Schema::table('invato_faq_categories', function($table)
        {
            $table->integer('questions_id')->unsigned();
        });
    }
}
