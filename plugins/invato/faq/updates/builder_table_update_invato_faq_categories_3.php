<?php namespace Invato\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateInvatoFaqCategories3 extends Migration
{
    public function up()
    {
        Schema::table('invato_faq_categories', function($table)
        {
            $table->boolean('login_required')->default(false);
        });
    }
    
    public function down()
    {
        Schema::table('invato_faq_categories', function($table)
        {
            $table->dropColumn('login_required');
        });
    }
}
