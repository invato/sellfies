<?php namespace Invato\Faq\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateInvatoFaqQuestions3 extends Migration
{
    public function up()
    {
        Schema::table('invato_faq_questions', function($table)
        {
            $table->renameColumn('categories_id', 'category_id');
        });
    }
    
    public function down()
    {
        Schema::table('invato_faq_questions', function($table)
        {
            $table->renameColumn('category_id', 'categories_id');
        });
    }
}
