<?php namespace Invato\Faq;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Invato\Faq\Components\Faq' => 'faq',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerPageSnippets()
    {
        return [
            'Invato\Faq\Components\Faq' => 'faq',
        ];
    }
}
