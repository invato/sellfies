<?php namespace Invato\Faq\Models;

use Model;

/**
 * Model
 */
class Question extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    use \October\Rain\Database\Traits\Sortable;

    protected $dates = ['deleted_at'];

    public $implement = ['RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['question', 'answer'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'invato_faq_questions';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'question'   => 'required',
        'answer'   => 'required',
    ];

    public $belongsTo = [
        'category' => \Invato\Faq\Models\Category::class
    ];
}
